<?php

namespace amianalien0x3f\EventLogBundle\Service;
use Psr\Log\LoggerInterface;

class ServiceInterface
{
    protected $logger;
    /**
     * Constructeur de la class
     */
    public function __construct(LoggerInterface $logger=null)
    {
        $this->logger = $logger;
    }
    public function setLogger(LoggerInterface $itf){
        $this->logger = $itf;
    }
    public function getLogger()
    {
        return $this->logger;
    }
}
