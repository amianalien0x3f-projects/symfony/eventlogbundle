<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace amianalien0x3f\EventLogBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent; 
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
/*use App\Controller\TokenAuthenticatedController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;*/
use Symfony\Component\HttpKernel\KernelEvents;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;

use amianalien0x3f\EventLogBundle\Entity\Event;

/**
 * Description of RequestEventSubscriber
 *
 * @author rv
 */
class RequestEventSubscriber implements  EventSubscriberInterface{
    
    protected $logger;
    protected $doctrine;
    protected $mobiledetector;
    protected $tokenStorage;
        /**
     * Constructeur de la class
     */
    public function __construct(LoggerInterface $logger=null,
                                EntityManagerInterface  $doctrine=null,
                                MobileDetector $mobiledetector,
                                TokenStorageInterface $tokenStorage)
    {
        $this->logger = $logger;
        $this->doctrine = $doctrine;
        $this->mobiledetector = $mobiledetector;
        $this->tokenStorage = $tokenStorage;
    }
    
    public function setLogger(LoggerInterface $itf){
        $this->logger = $itf;
    }
    public function getLogger() : LoggerInterface
    {
        return $this->logger;
    }
    public function setDoctrine(EntityManagerInterface $itf){
        $this->doctrine = $itf;
    }
    public function getDoctrine() : EntityManagerInterface 
    {
        return $this->doctrine;
    }
    
    public function setMobileDetector(MobileDetector $itf){
        $this->mobiledetector = $itf;
    }
    public function getMobileDetector() : MobileDetector 
    {
        return $this->mobiledetector;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
    public function onKernelController(FilterControllerEvent  $event)
    {
        $controller = $event->getController();
        $request = $event->getRequest();
        
        if($event->getRequestType() === HttpKernelInterface::MASTER_REQUEST){
            $metadata = [];
            $metadata['Headers'] = $request->headers->all();
            $metadata['ClientIp'] = $request->getClientIp();
            $metadata['Host'] = $request->getHost();
            $this->getLogger()->info(json_encode($metadata));
            $em = $this->getDoctrine();
            $mobile = $this->getMobileDetector();
            $repEvent= $em->getRepository('amianalien0x3fEventLogBundle:Event');
            $newEvent = new Event();
            $now = new \DateTime("now",new \DateTimeZone("UTC"));
            $newEvent->setDatetime($now);
            $newEvent->setMicrotime(microtime(TRUE));
            $newEvent->setTopic($request->getPathInfo());
            
            if ($token = $this->tokenStorage->getToken()) {
                if ($token->isAuthenticated()) {
                    if ($user = $token->getUser()) {
                        if(is_a($user,'Symfony\Component\Security\Core\User\UserInterface')) {
                            $newEvent->setUser($user->getUsername());
                        }
                    }
                }
            }
            $newEvent->setIp($request->getClientIp());
            $newEvent->setMetadata(json_encode($metadata));
            if($mobile->is('Firefox'))
            {
                $newEvent->setBrowser ('Firefox');
            }
            $em->persist($newEvent);
            $em->flush();
        }
        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        /*if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof TokenAuthenticatedController) {
            $token = $event->getRequest()->query->get('token');
            if (!in_array($token, $this->tokens)) {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
        }*/
    }
}
