<?php

namespace amianalien0x3f\EventLogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="eventlog_event")
 * @ORM\Entity(repositoryClass="amianalien0x3f\EventLogBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(name="datetime", type="datetimetz", nullable=false)
     */
    private $datetime;
    /**
     * @ORM\Column(name="microtime", type="float", nullable=false)
     */
    private $microtime;
    
    /**
     * @ORM\Column(name="ip", type="string", length=32, nullable=true)
     */
    private $ip;
    
    /**
     * @ORM\Column(name="topic", type="string", length=512, nullable=true)
     */
    private $topic;
    
    /**
     * @ORM\Column(name="user", type="string", length=512, nullable=true)
     */
    private $user;
    
    /**
     * @ORM\Column(name="device", type="string", length=32, nullable=true)
     */
    private $device;
    
    /**
     * @ORM\Column(name="os", type="string", length=32, nullable=true)
     */
    private $os;
    
    /**
     * @ORM\Column(name="browser", type="string", length=32, nullable=true)
     */
    private $browser;
    
    /**
     * @ORM\Column(name="metadata", type="string", length=4096, nullable=true)
     */
    private $metadata;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(?string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getDevice(): ?string
    {
        return $this->device;
    }

    public function setDevice(?string $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getOs(): ?string
    {
        return $this->os;
    }

    public function setOs(?string $os): self
    {
        $this->os = $os;

        return $this;
    }

    public function getBrowser(): ?string
    {
        return $this->browser;
    }

    public function setBrowser(?string $browser): self
    {
        $this->browser = $browser;

        return $this;
    }


    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Event
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set microtime
     *
     * @param float $microtime
     *
     * @return Event
     */
    public function setMicrotime($microtime)
    {
        $this->microtime = $microtime;

        return $this;
    }

    /**
     * Get microtime
     *
     * @return float
     */
    public function getMicrotime()
    {
        return $this->microtime;
    }

    /**
     * Set topic
     *
     * @param string $topic
     *
     * @return Event
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return string
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return Event
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }
}
