# EventLogBundle
Un event est identifié en base de donnée

id;datetime;group;campaign;type;sender;data;device;

<eventlog>/?event-group='daily-emails'&event-campaign='04/08/2019'&event-type='link-click'&event-sender='mysender@email.com'&event-data='{username:sendername,usergroup:sendergroup}'

Si besoin de cacher les paramètres, utiliser le paramètre footprint

<eventlog>/?event-footprint={{'daily-emails'&campaign='04/08/2019'&event='link-click'&sender='mysender@email.com'|md5}}

EventLog parameters.yml

prefix ='event-'