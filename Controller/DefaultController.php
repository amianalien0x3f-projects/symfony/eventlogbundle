<?php

namespace amianalien0x3f\EventLogBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;

class DefaultController extends Controller
{   /**
     * @Route("/", name="eventlog_homepage")
     */
    public function indexAction(Request $request,$activity=null)
    {
        $mobile_data = [];
        $mobile = $this->get('mobile_detect.mobile_detector');
        $mobile_data ['isMobile'] = $mobile->isMobile();
        $mobile_data ['isTablet'] = $mobile->isTablet();
        $mobile_data ['isWindows'] = $mobile->is('Windows');
        $mobile_data ['isFirefox'] = $mobile->is('Firefox');
        
        return $this->render('@amianalien0x3fEventLog/homepage.html.twig', ['mobile_data'=>$mobile_data]);
    }
}
